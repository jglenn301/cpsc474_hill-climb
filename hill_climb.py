import sys
import csv
import random

def score(p1, p2, values):
    p1_score = 0
    p2_score = 0

    for d1, d2, v in zip(p1, p2, values):
        if d1 > d2:
            p1_score += v
        elif d1 == d2:
            p1_score += v / 2
            p2_score += v / 2
        else:
            p2_score += v

    return (p1_score, p2_score)


def win(p1, p2, values):
    p1_score, p2_score = score(p1, p2, values)
    if p1_score > p2_score:
        return (1, 0)
    elif p1_score == p2_score:
        return (0.5, 0.5)
    else:
        return (0, 1)


def total(p1, entries, f, values):
    return sum(f(p1, p2, values)[0] for p2 in entries)


def next_step(curr, f, better):
    tie_count = 1
    best_value = f(curr)
    best_step = None

    # for all ways to move one unit from one battlefield to another
    for move_from in range(len(curr)):
        if curr[move_from] != 0:
            for move_to in range(len(curr)):
                if move_to != move_from:
                    # take the next step
                    step = curr[:]
                    step[move_from] -= 1
                    step[move_to] += 1

                    # check whether it is better than before
                    step_value = f(step)
                    if step_value == best_value:
                        tie_count += 1
                        if random.randint(1, tie_count) == 1:
                            best_step = step
                    elif better(step_value, best_value):
                        tie_count = 1
                        best_value = step_value
                        best_step = step
    return best_step
                    
    
def main():
    # read entries from standard input
    reader = csv.reader(sys.stdin)
    
    # skip header
    header = next(reader)

    # value of each battlefield: 1, 2, ..., battlefield-count = len(header)-1
    values = list(range(1, len(header)))

    # read all entries
    entries = [[int(c) for c in row[1:]] for row in reader]

    # score by wins
    evaluation = lambda p1: total(p1, entries, win, values)
    
    # start at a random entry
    curr = random.choice(entries)

    while curr is not None:
        prev = curr
        curr = next_step(curr, evaluation, lambda new, old: new > old)

    print(prev, evaluation(prev))
    

if __name__ == "__main__":
    main()
